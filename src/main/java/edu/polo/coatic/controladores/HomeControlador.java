package edu.polo.coatic.controladores;

import edu.polo.coatic.repositorios.*;
import edu.polo.coatic.servicios.*;
import jakarta.mail.MessagingException;

import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.core.context.SecurityContextHolder;

@RestController
public class HomeControlador {

    @Autowired
    CursoRepositorio cursoRepositorio;

    @Autowired
    CursoServicio cursoServicio;

    @Autowired
    EmailServicio emailServicio;

    @RequestMapping("/")
    public ModelAndView home()
    {
        ModelAndView maw = new ModelAndView();
        maw.setViewName("fragments/base");
        maw.addObject("titulo", "Inicio");
        maw.addObject("vista", "inicio/home");
        maw.addObject("cursos", cursoServicio.getAll());
        String destinatario = SecurityContextHolder.getContext().getAuthentication().getName();

        Map<String, Object> valores = new HashMap();
        valores.put("nombre", "Alejandro");
        valores.put("fecha", "05-09-2023");
        String[] habilidades = {"Programar", "Teclear mal", "Dormir"};
        valores.put("habilidades", habilidades);

        try {
            emailServicio.enviarMailHtml(destinatario, "Ensayo 70 de correo", "emails/ejemplo", valores);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return maw;  
    }
    
}